# Big Data Analytics of world project investors

The objective of this project is to analyze the amount of money assigned to 87 countries for projects funded by investors, as well as projects which did not find any investors. We discuss the key factors which lead investors to make their choices. The code is given as a Jupyter book.

## Introduction

The analysis is mainly based on the period in which the project will be running, the interval of payments, and the distribution of money per sector, e.g. agriculture, education, manufacturing, etc.
A second objective is to try to understand why some projects were partially or fully funded while others could not find investors. For this, we will define a risk factor as the product of the amount of money invested in funded projects and the period for the project. In the case of non-funded projects, we use the amount requested for non-funded projects.
We substantiate our analysis throughout three pairs of plots: two word-maps, two sunbursts, and two scatter plots with embedded histograms.

## Plot Analysis

The first world map "Total requested loan per Nation" clearly shows that the United States is the nation with the most non-funded projects. By hovering over this nation we can read 838, which is the number of non-funded projects. As a comparison, Mexico has 26, while the Philippines has only 8.
On the other hand, the second world map "Total amount funds" shows that the Philippines is the top country capturing money throughout funding -see bright yellow-, followed by Kenia, Peru, and Paraguay. Additionally, one can obtain the total number of projects by hovering, and these are respectively: 160329, 75273, 22204, and 11898.
The next two pairs of plots are in a sunburst style. The one on the left named “Risk Non-funded projects” shows which sectors the investors avoid among the non-funded projects. It is also clear that the "bullet" type of payment is avoided, and as we saw in the world map, the United States is the nation mostly covering the non-funded projects
The right sunburst plot named “Risk Funded projects” shows the preferred sectors for investors. The top three are agriculture, food, and retail. To see more clearly how the periods of payments are done, one needs to click on the sector which one is interested to analyze. It seems that the predominant way of payment is monthly.
One very important aspect one can learn from these two sunburst plots is that non-funded projects receive no attention from investors because they have a high-risk factor. This can be seen by the lighter color of the “Risk Non-funded projects” if compared with the “Risk Funded projects” (see the bar scale measured in dollar-months units).
For an exact assessment of the repayment interval and sector investments, we plot in the last two scatter figures embedded histograms for each category of the repayment interval, e.g. “bullet”, “monthly”, “weakly”, and “irregular”. The levels of the histograms correspond to the contribution of each sector. Here we can more clearly see that the monthly payments are the most common while the weekly is distinctly very low for the funded projects. The size of the points indicates the number of projects for a sector at a given term in months and repayment intervals.
The scatter points also inform about the time for the projects. Here we see that the irregular payments have the longest periods of money return
